<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TokenLogin\AuthController;
use App\Http\Controllers\Article\ArticleController;
use App\Http\Controllers\Preference\SourceController;
use App\Http\Controllers\Preference\AuthorController;
use App\Http\Controllers\Preference\CategoryController;
use App\Http\Controllers\Preference\UserPreferenceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:api')->group(function (){

    Route::get('/logout',[AuthController::class,'logout']);
//    Route::get('/get-articles',[ArticleController::class,'getArticles'])->middleware('customize.articles',);

    Route::post('/user-preferences',[UserPreferenceController::class,'store']);
    Route::get('/get-preferences',[UserPreferenceController::class,'index']);


});
Route::get('/get-articles',[ArticleController::class,'getArticles']);
Route::get('/get-sources',[SourceController::class,'index']);
Route::get('/get-authors',[AuthorController::class,'index']);
Route::get('/get-categories',[CategoryController::class,'index']);



Route::post('/login',[AuthController::class,'login']);
Route::post('/register',[AuthController::class,'register']);

Route::get('/test', function () {
        dd(\App\Services\NewsAPIService::getArticles());

});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
