# FROM php:8.1-cli

# RUN apt-get update && apt-get install -y \
#     libpng-dev \
#     libonig-dev \
#     libxml2-dev \
#     libzip-dev \
#     zip \
#     unzip

# RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd zip

# COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# # Copy existing application directory contents
# COPY . .

# # Install composer dependencies
# RUN composer install --no-interaction --optimize-autoloader --no-scripts

# # Generate encryption key
# RUN php artisan key:generate

# RUN composer require laravel/passport
# RUN php artisan passport:keys --force

# CMD ["php", "artisan", "serve", "--port=8000"]


# Base image
FROM php:8.1-cli

# Install dependencies
RUN apt-get update && \
    apt-get install -y \
        git \
        zip \
        unzip \
        libpng-dev \
        libonig-dev \
        libxml2-dev \
        libzip-dev \
        libpq-dev \
        default-mysql-client \
        && docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd zip

# Set the working directory
WORKDIR /backend

# Install composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Copy application files
COPY . .

# Install project dependencies
RUN composer install --no-dev --no-scripts --optimize-autoloader

# Expose port
EXPOSE 8000

# Start the PHP development server
CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=8000"]
