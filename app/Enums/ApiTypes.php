<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self newsapi()
 * @method static self theguardian()
 * @method static self nytimes()
 */
class ApiTypes extends Enum
{
    protected static function values(): array
    {
        return [
            'newsApi' => 'newsApi',
            'theGuardian' => 'theGuardian',
            'nyTimes' => 'nyTimes',
        ];
    }
}
