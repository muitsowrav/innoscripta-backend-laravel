<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $guarded=[];

    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function savedByUsers()
    {
        return $this->belongsToMany(User::class, 'user_saved_articles');
    }


    public function scopeSearch($query, $search)
    {
        if ($search) {
            collect(explode(' ', $search))->filter()->each(function ($term) use ($query) {
                $term = '%' . $term . '%'; // Wildcard symboil for partiall matching

                $query->where('title', 'like', $term)

//                    ->orWhereHas('source', function ($query) use ($term) {
//                        $query->where('name', 'like', $term);
//                    })
//                    ->orWhere('description', 'like', $term)
//                    ->orWhere('content', 'like', $term)
                     ;

            });
        }
    }

}
