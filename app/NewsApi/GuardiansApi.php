<?php

namespace App\NewsApi;

use App\Services\GrudiansService;

class GuardiansApi implements NewsApiInterface
{
    public function fetchNews(): array
    {
       $articles=GrudiansService::getArticles();
       //dd($articles);
       return GrudiansService::processArticles($articles);
    }
}
