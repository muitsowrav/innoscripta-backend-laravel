<?php

namespace App\NewsApi;

use App\Services\NewsAPIService;

class NewsApiOrg implements NewsApiInterface
{
    public function fetchNews(): array
    {
        $articles= NewsAPIService::getArticles();

        return NewsAPIService::processArticles($articles);
    }
}
