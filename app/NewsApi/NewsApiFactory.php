<?php

namespace App\NewsApi;

use App\Enums\ApiTypes;

class NewsApiFactory
{
    public static function createNewsApi(string $type): NewsApiInterface
    {
        switch ($type) {

            case ApiTypes::newsapi():
                return new NewsApiOrg();
            case ApiTypes::theguardian():
                return new GuardiansApi();
            case ApiTypes::nytimes():
                return  new NyTimesApi();
            default:
                throw new \InvalidArgumentException('Invalid API type');
        }
    }
}
