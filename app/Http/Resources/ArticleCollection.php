<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ArticleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {

        return [
            'success' => true,
            'message' => 'Article list',
            'code' => 200,
            'data' => [
                'count'=>$this->collection->count(),
                'article' => ArticleResource::collection($this->collection)
            ],
        ];
    }
}
