<?php

namespace App\Http\Middleware;

use App\Models\Article;
use App\Models\Category;
use App\Models\Source;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CustomizeArticles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        $search = $request->search;
        $date = ($request->date && $request->date != 'null') ? Carbon::parse($request->date) : Carbon::today();
        $category=( $request->category) ?[ $request->category] : Category::all()->pluck('id');
        $source=( $request->source) ? [$request->source] : Source::all()->pluck('id');

        dd(Auth::user());
        if (Auth::check()) {

            $user = Auth::user();
            dd($user);
            $articles=Article::all();



        } else {


            $articles=Article::with('source','category','author')
                ->whereIn('category_id',$category)
                ->whereIn('source_id',$source)
                ->whereDate('created_at',$date)
                ->search($request->search)
                ->get();


        }
        // Pass the general articles to the request

        $request->merge([
            'articles' => $articles,
        ]);
        return $next($request);
    }
}
