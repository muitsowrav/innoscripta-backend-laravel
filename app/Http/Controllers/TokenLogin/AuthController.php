<?php

namespace App\Http\Controllers\TokenLogin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Token;

class AuthController extends Controller
{
    use ApiResponse;

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        if (Auth::attempt($credentials)) {
            $user = Auth::user();

            $token = $user->createToken(env('APP_NAME'))->accessToken;


            return $this->success([
                'token' => $token
            ], 'login Successfull', 200);
        } else {
            return $this->error('Unauthorized'
                , 'Unauthorized', 401);
        }
    }

    public function register(RegisterRequest $request)
    {
//        dd($request);
        // The request has already been validated

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        $token = $user->createToken('Innoscripta')->accessToken;

        return $this->success([
            'token' => $token
        ], ' Successfully Registered ', 201);    }


    public function logout(Request $request)
    {
        $accessToken = Auth::user()->token();

        // Revoke the access token
//        dd('okkk');
        $accessToken->revoke();

        // Delete the access token from the database
        Token::where('id', $accessToken->id)->delete();

        return $this->success(null, 'Logout Successful', 200);
    }
}
