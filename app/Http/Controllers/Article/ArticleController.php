<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleCollection;
use App\Models\Article;
use App\Models\Category;
use App\Models\Source;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    public function getArticles(Request $request)
    {

//        dd($request->all());
        $user = Auth::guard('api')->user();
        $search = $request->search;
        $date = ($request->date && $request->date != 'null') ? Carbon::parse($request->date) : Carbon::today();
        $category = ($request->category) ? $request->category : Category::all()->pluck('id');
        $source = ($request->source) ? $request->source : Source::all()->pluck('id');
        $articles = Article::query();
        if ($user) {
            $userPreferences = $user->preferences;

            if ($userPreferences->isNotEmpty()) {
                $preferenceIds = $userPreferences->pluck('preferenceable_id');
                $preferenceTypes = $userPreferences->pluck('preferenceable_type');

                $this->addPreferenceConditions($articles, $preferenceTypes, $preferenceIds);
            }
        }

        $resultantArticles = $articles->with('source', 'category', 'author')
            ->whereIn('category_id', $category)
            ->whereIn('source_id', $source)
            ->whereDate('created_at', $date)
            ->search($search)
            ->paginate(10);
//dd($resultantArticles);
        return new ArticleCollection($resultantArticles);
    }

    private function addPreferenceConditions($query, $preferenceTypes, $preferenceIds)
    {
        $query->where(function ($subQuery) use ($preferenceTypes, $preferenceIds) {
            foreach ($preferenceTypes as $index => $type) {
                if ($type == 'App\Models\Source') {
                    $subQuery->orWhere('source_id', $preferenceIds[$index]);
                } elseif ($type == 'App\Models\Category') {
                    $subQuery->orWhere('category_id', $preferenceIds[$index]);
                } elseif ($type == 'App\Models\Author') {
                    $subQuery->orWhere('author_id', $preferenceIds[$index]);
                }
            }
        });
    }
}
