<?php

namespace App\Http\Controllers\Preference;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemCollection;
use App\Models\Source;
use App\Traits\ApiResponse;

class SourceController extends Controller

{

    use ApiResponse;

    public function index()
    {
        return $this->success(new ItemCollection(
            Source::all()
        ), 'Source Lists', 200);
    }
}
