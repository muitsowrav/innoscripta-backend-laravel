<?php

namespace App\Http\Controllers\Preference;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemCollection;
use App\Models\Category;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ApiResponse;
    public function index()
    {
        return $this->success(new ItemCollection(
            Category::all()
        ), 'Source Lists', 200);
    }
}
